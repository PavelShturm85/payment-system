#!/usr/bin/env bash

pylint payment_system, tests
mypy --config-file mypy.ini .
