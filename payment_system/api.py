"""Payments system API."""
import fastapi
from fastapi import Depends

from payment_system.models import requests, responses
from payment_system.views import clients, wallets


ROUTER_OBJ: fastapi.APIRouter = fastapi.APIRouter()


@ROUTER_OBJ.post(
    "/clients/",
    response_model=responses.NewClientModel,
    status_code=fastapi.status.HTTP_201_CREATED,
)
async def post_create_clients(
    client_model: requests.NewClientModel = fastapi.Body(...),
    client_view: clients.CreateClientsView = Depends(clients.CreateClientsView),
) -> responses.NewClientModel:
    """Create new client with wallet."""
    return await client_view.create_new_client_with_wallet(client_model)


@ROUTER_OBJ.post(
    "/wallets/{wallet_id}/fill_up/",
    response_model=responses.BaseSuccessModel,
    status_code=fastapi.status.HTTP_200_OK,
)
async def post_top_up_balance(
    wallet_id: int = fastapi.Path(...),
    fill_up_wallet_model: requests.WalletFillUpModel = fastapi.Body(...),
    top_up_balance_view: wallets.TopUpBalanceViews = Depends(wallets.TopUpBalanceViews),
) -> responses.BaseSuccessModel:
    """Top up wallet balance."""
    await top_up_balance_view.fill_up_wallet(wallet_id, fill_up_wallet_model)
    return responses.BaseSuccessModel()


@ROUTER_OBJ.post(
    "/wallets/transfer/",
    response_model=responses.BaseSuccessModel,
    status_code=fastapi.status.HTTP_200_OK,
)
async def post_transfer_balance(
    transfer_model: requests.TransferModel = fastapi.Body(...),
    transfer_view: wallets.TransferBalanceViews = Depends(wallets.TransferBalanceViews),
) -> responses.BaseSuccessModel:
    """Currency transfers between clients."""
    await transfer_view.transfer_funds(transfer_model)
    return responses.BaseSuccessModel()
