"""Business models."""
import datetime
import decimal

import sqlalchemy as sa
from sqlalchemy.orm import registry
from sqlalchemy.orm.decl_api import DeclarativeMeta


mapper_registry = registry()


class Base(metaclass=DeclarativeMeta):
    __abstract__ = True

    registry = mapper_registry
    metadata = mapper_registry.metadata


class Clients(Base):
    __tablename__ = 'clients'

    id: int = sa.Column(sa.Integer, primary_key=True)
    first_name: str = sa.Column(sa.String(64), nullable=False)
    last_name: str = sa.Column(sa.String(64), nullable=False)


class Wallets(Base):
    __tablename__ = 'wallets'

    id: int = sa.Column(sa.Integer, primary_key=True)
    client_id: int = sa.Column(sa.Integer, sa.ForeignKey('clients.id'))
    currency_type: str = sa.Column(sa.String(3), nullable=False)
    bankroll: decimal.Decimal = sa.Column(sa.DECIMAL(19, 2), server_default='0.00')


class Transactions(Base):
    __tablename__ = 'transactions'

    id: int = sa.Column(sa.BigInteger, primary_key=True)
    payer_id: int = sa.Column(sa.Integer, sa.ForeignKey('wallets.id'), nullable=True)
    recipient_id: int = sa.Column(sa.Integer, sa.ForeignKey('wallets.id'))
    transfer: decimal.Decimal = sa.Column(sa.DECIMAL(19, 2), nullable=False)
    time_stamp: datetime.datetime = sa.Column(sa.DateTime(timezone=True), server_default=sa.sql.func.now())
