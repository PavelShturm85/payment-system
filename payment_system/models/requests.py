"""Request models."""
import decimal
import typing

import pydantic

from payment_system import settings
from payment_system.models.entities import CurrencyTypeEnum


class NewClientModel(pydantic.BaseModel):
    """Request client model."""

    first_name: str = pydantic.Field(..., min_length=2, max_length=64)
    last_name: str = pydantic.Field(..., min_length=2, max_length=64)
    currency_type: CurrencyTypeEnum

    class Config:
        """Config class."""

        use_enum_values: bool = True


class TransferModel(pydantic.BaseModel):
    """Request transfer model."""

    payer_wallet_id: typing.Optional[int]
    recipient_wallet_id: typing.Optional[int]
    transfer: pydantic.condecimal(gt=decimal.Decimal(settings.TRANSFER_AMOUNT_GT), decimal_places=2)  # type: ignore


class WalletFillUpModel(pydantic.BaseModel):
    """Fill up wallet model."""

    replenishment_amount: pydantic.condecimal(gt=decimal.Decimal(settings.FILL_UP_AMOUNT_GT), decimal_places=2)  # type: ignore
