"""Response models."""
import decimal

import pydantic

from payment_system.models.requests import CurrencyTypeEnum


class NewClientModel(pydantic.BaseModel):
    """Response new client model."""

    client_id: int
    wallet_id: int
    first_name: str
    last_name: str
    currency_type: CurrencyTypeEnum
    bankroll: pydantic.condecimal(ge=decimal.Decimal(0.00), decimal_places=2)  # type: ignore


class BaseSuccessModel(pydantic.BaseModel):
    """Base success response model."""

    status: int = 200
    message: str = "success"
