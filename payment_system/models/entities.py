"""Request models."""
import datetime
import decimal
import enum

import pydantic


class CurrencyTypeEnum(enum.Enum):
    """Currency type enum."""

    USD: str = "USD"


class ClientModel(pydantic.BaseModel):
    """Client model."""

    id: int
    first_name: str
    last_name: str


class WalletsModel(pydantic.BaseModel):
    """Wallet model."""

    id: int
    client_id: int
    currency_type: CurrencyTypeEnum
    bankroll: decimal.Decimal

    class Config:
        """Config class."""

        use_enum_values: bool = True


class TransactionsModel(pydantic.BaseModel):
    """Transaction model."""

    id: int
    payer_id: int
    recipient_id: int
    transfer: decimal.Decimal
    time_stamp: datetime.datetime
