"""Wallets views."""
import logging

import asyncpg
import fastapi
from fastapi import Depends

from payment_system import connections
from payment_system.models import requests
from payment_system.repositories import decorators, transactions, wallets


ROUTER_OBJ: fastapi.APIRouter = fastapi.APIRouter()
LOGGER: logging.Logger = logging.getLogger(__name__)


class TopUpBalanceViews:
    """Top up balance views."""

    def __init__(
        self,
        wallets_repository: wallets.WalletsRepository = Depends(
            wallets.WalletsRepository),
        transactions_repository: transactions.TransactionsRepository = Depends(
            transactions.TransactionsRepository),
        transaction_manager: connections.DatabaseConnection = Depends(
            connections.DatabaseConnection),
    ) -> None:
        self.wallets_repository: wallets.WalletsRepository = wallets_repository
        self.transactions_repository: transactions.TransactionsRepository = transactions_repository
        self.transaction_manager: connections.DatabaseConnection.connection.transaction = (  # type: ignore
            transaction_manager.connection.transaction
        )

    @decorators.transaction_retry
    async def fill_up_wallet(self, wallet_id: int, fill_up_wallet_model: requests.WalletFillUpModel) -> None:
        """Fill up wallet."""
        async with self.transaction_manager(isolation="read_commited"):
            await self.wallets_repository.is_exists_wallet(wallet_id)
            await self.wallets_repository.fill_up_wallet(wallet_id, fill_up_wallet_model.replenishment_amount)
            await self.transactions_repository.create_transaction(wallet_id, fill_up_wallet_model.replenishment_amount)


class TransferBalanceViews:
    """Transfer fund view."""

    def __init__(
        self,
        wallets_repository: wallets.WalletsRepository = Depends(
            wallets.WalletsRepository),
        transactions_repository: transactions.TransactionsRepository = Depends(
            transactions.TransactionsRepository),
        transaction_manager: connections.DatabaseConnection = Depends(
            connections.DatabaseConnection),
    ) -> None:
        self.wallets_repository: wallets.WalletsRepository = wallets_repository
        self.transactions_repository: transactions.TransactionsRepository = transactions_repository
        self.transaction_manager: connections.DatabaseConnection.connection.transaction = (  # type: ignore
            transaction_manager.connection.transaction
        )

    @decorators.transaction_retry
    async def transfer_funds(self, transaction_model: requests.TransferModel) -> None:
        """Transfer funds."""
        async with self.transaction_manager(isolation="read_commited"):
            await self.wallets_repository.is_exists_wallet(transaction_model.payer_wallet_id)
            is_withdraw: bool = await self.wallets_repository.withdraw_wallet(
                transaction_model.payer_wallet_id, transaction_model.transfer
            )
            if not is_withdraw:
                not_withdraw: str = "Insufficient funds on the account."
                LOGGER.error(not_withdraw)
                raise fastapi.HTTPException(
                    detail=not_withdraw,
                    status_code=fastapi.status.HTTP_424_FAILED_DEPENDENCY,
                )
            await self.wallets_repository.is_exists_wallet(transaction_model.recipient_wallet_id)
            await self.wallets_repository.fill_up_wallet(
                transaction_model.recipient_wallet_id, transaction_model.transfer
            )
            await self.transactions_repository.create_transaction(
                transaction_model.recipient_wallet_id, transaction_model.transfer, transaction_model.payer_wallet_id
            )
