"""Clients views."""
import logging

import asyncpg
import fastapi
from fastapi import Depends

from payment_system import connections
from payment_system.models import entities, requests, responses
from payment_system.repositories import clients, decorators, wallets


LOGGER: logging.Logger = logging.getLogger(__name__)


class CreateClientsView:
    """Create client views."""

    def __init__(
        self,
        clients_repository: clients.ClientsRepository = Depends(clients.ClientsRepository),
        wallets_repository: wallets.WalletsRepository = Depends(wallets.WalletsRepository),
        transaction_manager: connections.DatabaseConnection = Depends(connections.DatabaseConnection),
    ):
        self.clients_repository: clients.ClientsRepository = clients_repository
        self.wallets_repository: wallets.WalletsRepository = wallets_repository
        self.transaction_manager: connections.DatabaseConnection.connection.transaction = (  # type: ignore
            transaction_manager.connection.transaction
        )

    @decorators.transaction_retry
    async def create_new_client_with_wallet(self, client_model: requests.NewClientModel) -> responses.NewClientModel:
        """Create new client with wallet."""

        async with self.transaction_manager(isolation="read_committed"):
            try:
                new_client: entities.ClientModel = await self.clients_repository.create_client(client_model)
            except asyncpg.exceptions.UniqueViolationError as exc:
                client_exists_message: str = (
                    f"Client with first_name {client_model.first_name}"
                    f" and last_name {client_model.last_name} already exists."
                )
                LOGGER.info(client_exists_message)
                raise fastapi.HTTPException(
                    detail=client_exists_message,
                    status_code=fastapi.status.HTTP_409_CONFLICT,
                ) from exc
            new_wallet: entities.WalletsModel = await self.wallets_repository.create_wallet(
                new_client.id, client_model.currency_type
            )

        return responses.NewClientModel(
            client_id=new_client.id,
            wallet_id=new_wallet.id,
            first_name=new_client.first_name,
            last_name=new_client.last_name,
            currency_type=new_wallet.currency_type,
            bankroll=new_wallet.bankroll,
        )
