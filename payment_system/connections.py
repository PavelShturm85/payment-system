"""Connections."""
from databases import Database

from payment_system import settings


class DatabaseConnection:
    """Database connection."""

    connection: Database

    def __new__(cls):
        if not hasattr(cls, "instance"):
            cls.instance = super().__new__(cls)
        return cls.instance

    @classmethod
    async def initialize(cls):
        """Initialize data base connection."""
        cls.connection: Database = Database(
            settings.DATABASE_DSN,
            min_size=settings.DATABASE_POOL_MIN_SIZE,
            max_size=settings.DATABASE_POOL_MAX_SIZE,
        )
        await cls.connection.connect()

    @classmethod
    async def shutdown(cls) -> None:
        """Close data base connection."""
        if cls.connection:
            await cls.connection.disconnect()
