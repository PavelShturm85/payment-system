"""Settings for project."""
import envparse


DATABASE_DSN: str = envparse.env("DATABASE_DSN", "postgresql://127.0.0.1:5432/payment_system")
DATABASE_POOL_MIN_SIZE: int = envparse.env("DATABASE_POOL_MIN_SIZE", 10, cast=int)
DATABASE_POOL_MAX_SIZE: int = envparse.env("DATABASE_POOL_MAX_SIZE", 30, cast=int)
DATABASE_CONNECTION_TRIES: int = envparse.env("DATABASE_CONNECTION_TRIES", cast=int, default=3)
DATABASE_TRANSACTIONS_TRIES: int = envparse.env("DATABASE_TRANSACTIONS_TRIES", cast=int, default=3)

DOC_PREFIX: str = envparse.env("DOC_PREFIX", "/doc")
API_PREFIX: str = envparse.env("API_PREFIX", "/api")
TAGS_METADATA: list = [
    {"name": "payments", "description": "Methods that interacts with payments."},
]

UVICORN: dict = {
    "APP_PORT": envparse.env("APP_PORT", default=9991, cast=int),
    "LOG_LEVEL": envparse.env("LOG_LEVEL", "info"),
    "APP_WORKERS": envparse.env("APP_WORKERS", default=5, cast=int),
    "APP_LIMIT_MAX_REQUESTS": envparse.env("APP_LIMIT_MAX_REQUESTS", default=5000, cast=int),
    "APP_LOOP": envparse.env("APP_LOOP", "uvloop"),
}

FILL_UP_AMOUNT_GT: float = envparse.env("FILL_UP_AMOUNT_GT", 0.00, cast=float)
TRANSFER_AMOUNT_GT: float = envparse.env("TRANSFER_AMOUNT_GT", 0.00, cast=float)
