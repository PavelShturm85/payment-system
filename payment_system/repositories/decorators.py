"""Database-specific decorators."""
import functools

import asyncpg
import backoff

from payment_system import settings


def postgres_reconnect(func):
    """Decorator for postgres reconnection."""

    @backoff.on_exception(
        backoff.expo,
        (asyncpg.PostgresConnectionError,),
        max_tries=settings.DATABASE_CONNECTION_TRIES,
    )
    @functools.wraps(func)
    async def wrapped_method(*args, **kwargs):
        return await func(*args, **kwargs)

    return wrapped_method


def transaction_retry(func):
    """Decorator for transaction reconnection."""

    @backoff.on_exception(
        backoff.expo,
        (asyncpg.SerializationError,),
        max_tries=settings.DATABASE_TRANSACTIONS_TRIES,
    )
    @functools.wraps(func)
    async def wrapped_method(*args, **kwargs):
        return await func(*args, **kwargs)

    return wrapped_method
