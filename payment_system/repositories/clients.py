"""Repository for use clients entities model."""
import typing

import sqlalchemy as sa

from payment_system.models import entities, requests
from payment_system.models.db_schemas import Clients
from payment_system.repositories import base, decorators


class ClientsRepository(base.BaseRepository):
    """Repository for client table."""

    @decorators.postgres_reconnect
    async def create_client(self, client_model: requests.NewClientModel) -> typing.Optional[entities.ClientModel]:
        """Create new client."""
        mapping_result: typing.Optional[typing.Mapping] = await self.database_connection.fetch_one(
            sa.insert(Clients)
            .values(client_model.dict(exclude={"currency_type"}))
            .returning(Clients.id, Clients.first_name, Clients.last_name)
        )
        if not mapping_result:
            return None
        return entities.ClientModel(**dict(mapping_result))
