"""Repository for use transactions entities model."""
import decimal
import typing

import sqlalchemy as sa

from payment_system.models.db_schemas import Transactions
from payment_system.repositories import base, decorators


class TransactionsRepository(base.BaseRepository):
    """Transactions for wallets table."""

    @decorators.postgres_reconnect
    async def create_transaction(
        self, recipient_id: int, transfer: decimal.Decimal, payer_id: typing.Optional[int] = None
    ) -> None:
        """Create transaction."""
        await self.database_connection.execute(
            sa.insert(Transactions).values(payer_id=payer_id, recipient_id=recipient_id, transfer=transfer)
        )
