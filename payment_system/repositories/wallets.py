"""Repository for use wallets entities model."""
import decimal
import logging
import typing

import fastapi
import sqlalchemy as sa
from sqlalchemy import and_

from payment_system.models import entities
from payment_system.models.db_schemas import Wallets
from payment_system.repositories import base, decorators


LOGGER: logging.Logger = logging.getLogger(__name__)


class WalletsRepository(base.BaseRepository):
    """Repository for wallets table."""

    @decorators.postgres_reconnect
    async def is_exists_wallet(self, wallet_id: int) -> None:
        """Is exists wallet."""
        result: typing.Optional[typing.Mapping] = await self.database_connection.execute(
            sa.select(sa.exists(Wallets)).where(Wallets.id == wallet_id)
        )
        if not result:
            not_wallet: str = f"Not exists wallet with id {wallet_id}."
            LOGGER.error(not_wallet)
            raise fastapi.HTTPException(
                detail=not_wallet,
                status_code=fastapi.status.HTTP_404_NOT_FOUND,
            )

    @decorators.postgres_reconnect
    async def fill_up_wallet(self, wallet_id: int, replenishment_amount: decimal.Decimal) -> None:
        """Fill up wallet."""
        await self.database_connection.execute(
            sa.update(Wallets).where(Wallets.id == wallet_id).values(bankroll=Wallets.bankroll + replenishment_amount)
        )

    @decorators.postgres_reconnect
    async def withdraw_wallet(self, wallet_id: int, withdraw_amount: decimal.Decimal) -> bool:
        """Withdraw wallet."""
        result: typing.Optional[typing.Mapping] = await self.database_connection.execute(
            sa.update(Wallets)
            .where(and_(Wallets.id == wallet_id, Wallets.bankroll >= withdraw_amount))
            .values(bankroll=Wallets.bankroll - withdraw_amount)
            .returning(1)
        )
        return bool(result)

    @decorators.postgres_reconnect
    async def create_wallet(
        self, client_id: int, currency_type: entities.CurrencyTypeEnum
    ) -> typing.Optional[entities.WalletsModel]:
        """Create new wallet."""
        mapping_result: typing.Optional[typing.Mapping] = await self.database_connection.fetch_one(
            sa.insert(Wallets)
            .values(client_id=client_id, currency_type=currency_type)
            .returning(Wallets.id, Wallets.client_id, Wallets.currency_type, Wallets.bankroll)
        )
        if not mapping_result:
            return None
        return entities.WalletsModel(**mapping_result)
