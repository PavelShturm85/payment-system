"""Entrypoint for dialogs API."""
import fastapi
from uvicorn import Config, Server

from payment_system import api, connections, settings


async def startup():
    """Global initialization."""
    await connections.DatabaseConnection.initialize()


async def shutdown():
    """Global shutdown."""
    await connections.DatabaseConnection.shutdown()


APP_OBJ: fastapi.FastAPI = fastapi.FastAPI(
    docs_url=settings.DOC_PREFIX,
    on_startup=[startup],
    on_shutdown=[shutdown],
    openapi_url=f"{settings.API_PREFIX}/openapi.json",
    openapi_tags=settings.TAGS_METADATA,
)
APP_OBJ.include_router(api.ROUTER_OBJ, prefix=settings.API_PREFIX, tags=["payments"])


if __name__ == "__main__":
    uvicorn_server: Server = Server(
        Config(
            APP_OBJ,
            host="0.0.0.0",
            port=settings.UVICORN["APP_PORT"],
            log_level=settings.UVICORN["LOG_LEVEL"],
            workers=settings.UVICORN["APP_WORKERS"],
            loop=settings.UVICORN["APP_LOOP"],
            limit_max_requests=settings.UVICORN["APP_LIMIT_MAX_REQUESTS"],
        )
    )
    uvicorn_server.run()
