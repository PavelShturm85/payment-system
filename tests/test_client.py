"""Tests for clients API."""
import asyncpg
import fastapi
from requests import Response

from payment_system.repositories import clients, wallets
from tests import helpers
from tests import mocks as custom_mock


API_CLIENTS: str = "/api/clients/"


def test_client(app: fastapi.FastAPI) -> None:
    """Test client positive."""
    app.dependency_overrides[clients.ClientsRepository] = lambda: custom_mock.MockClientsRepository()
    app.dependency_overrides[wallets.WalletsRepository] = lambda: custom_mock.MockWalletsRepository()

    response: Response = helpers.base_test_logic(API_CLIENTS, app, helpers.generate_request_client_model().json())

    assert response.status_code == 201


def test_not_unique_client(app: fastapi.FastAPI) -> None:
    """Test not unique client."""
    app.dependency_overrides[clients.ClientsRepository] = lambda: custom_mock.MockClientsRepository(
        error=asyncpg.exceptions.UniqueViolationError
    )

    response: Response = helpers.base_test_logic(API_CLIENTS, app, helpers.generate_request_client_model().json())

    assert response.status_code == 409


def test_not_valid_request(app: fastapi.FastAPI) -> None:
    """Test not valid request."""
    response: Response = helpers.base_test_logic(API_CLIENTS, app, helpers.generate_not_valid_request())

    assert response.status_code == 422
