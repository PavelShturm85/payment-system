"""Basic tests."""
import pytest

from payment_system import __main__ as api_main
from tests import mocks as custom_mock


def test_is_singleton_connection():
    """Test is singleton connection."""
    one_obj: custom_mock.BaseMockDataBase = custom_mock.BaseMockDataBase()
    other_obj: custom_mock.BaseMockDataBase = custom_mock.BaseMockDataBase()
    assert one_obj is other_obj


@pytest.mark.asyncio
async def test_startup_and_shutdown(monkeypatch):
    """Test startup and shutdown."""
    monkeypatch.setattr('payment_system.connections.Database', custom_mock.BaseMockDataBase)
    await api_main.startup()
    await api_main.shutdown()
