"""Tests for wallet API."""
import fastapi
from requests import Response

from payment_system.repositories import wallets
from tests import helpers
from tests import mocks as custom_mock


API_CLIENTS: str = "/api/wallets/{}/fill_up/"


def test_fill_up(app: fastapi.FastAPI) -> None:
    """Test fill up positive."""
    app.dependency_overrides[wallets.WalletsRepository] = lambda: custom_mock.MockWalletsRepository()
    response: Response = helpers.base_test_logic(
        API_CLIENTS.format(helpers.generate_random_int()), app, helpers.generate_request_fill_up_wallet_model().json()
    )

    assert response.status_code == 200


def test_not_valid_request(app: fastapi.FastAPI) -> None:
    """Test wallet not valid request."""
    app.dependency_overrides[wallets.WalletsRepository] = lambda: custom_mock.MockWalletsRepository()
    response: Response = helpers.base_test_logic(
        API_CLIENTS.format(helpers.generate_random_int()), app, helpers.generate_not_valid_request()
    )

    assert response.status_code == 422
