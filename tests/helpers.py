"""Helpers for tests."""
import json
import typing

import fastapi
from faker import Faker
from fastapi.testclient import TestClient
from requests import Response

from payment_system.models import entities, requests


FAKE_OBJ: Faker = Faker()


def base_test_logic(api: str, app: fastapi.FastAPI, request: str) -> Response:
    """Base test logic."""
    client: TestClient = TestClient(app)
    return client.post(api, data=request)


def generate_request_client_model() -> requests.NewClientModel:
    """Generate request client model."""
    return requests.NewClientModel(
        first_name=FAKE_OBJ.name(), last_name=FAKE_OBJ.last_name(), currency_type=requests.CurrencyTypeEnum.USD
    )


def generate_request_fill_up_wallet_model() -> requests.WalletFillUpModel:
    """Generate request fill up model."""
    return requests.WalletFillUpModel(replenishment_amount=FAKE_OBJ.pydecimal(right_digits=2, min_value=0))


def generate_request_transfer_wallet_model() -> requests.TransferModel:
    """Generate request transfer model."""
    return requests.TransferModel(
        payer_wallet_id=generate_random_int(),
        recipient_wallet_id=generate_random_int(),
        transfer=FAKE_OBJ.pydecimal(right_digits=2, min_value=0),
    )


def generate_entities_client_model() -> typing.Optional[entities.ClientModel]:
    """Generate entities client model."""
    return entities.ClientModel(
        id=generate_random_int(),
        first_name=FAKE_OBJ.name(),
        last_name=FAKE_OBJ.last_name(),
    )


def generate_not_valid_request() -> str:
    """Generate not valid request."""
    return json.dumps(
        {FAKE_OBJ.currency_code(): FAKE_OBJ.currency_name() for _ in range(FAKE_OBJ.random.randint(2, 6))}
    )


def generate_random_int() -> int:
    """Generate random int."""
    return FAKE_OBJ.random.randint(1, 10000)


def generate_entities_wallet_model() -> entities.WalletsModel:
    """Generate entities wallet model."""
    return entities.WalletsModel(
        id=generate_random_int(),
        client_id=generate_random_int(),
        currency_type=requests.CurrencyTypeEnum.USD,
        bankroll=FAKE_OBJ.pydecimal(right_digits=2, min_value=0),
    )
