"""Tests for wallet API."""
import fastapi
from requests import Response

from payment_system.repositories import wallets
from tests import helpers
from tests import mocks as custom_mock


API_CLIENTS: str = "/api/wallets/transfer/"


def test_transfer(app: fastapi.FastAPI) -> None:
    """Test transfer positive."""
    app.dependency_overrides[wallets.WalletsRepository] = lambda: custom_mock.MockWalletsRepository()
    response: Response = helpers.base_test_logic(
        API_CLIENTS.format(helpers.generate_random_int()), app, helpers.generate_request_transfer_wallet_model().json()
    )

    assert response.status_code == 200


def test_not_valid_request(app: fastapi.FastAPI) -> None:
    """Test wallet not valid request."""
    app.dependency_overrides[wallets.WalletsRepository] = lambda: custom_mock.MockWalletsRepository()
    response: Response = helpers.base_test_logic(
        API_CLIENTS.format(helpers.generate_random_int()), app, helpers.generate_not_valid_request()
    )

    assert response.status_code == 422


def test_insufficient_funds_for_transfer(app: fastapi.FastAPI) -> None:
    """Test insufficient funds for transfer."""
    app.dependency_overrides[wallets.WalletsRepository] = lambda: custom_mock.MockWalletsRepository(withdraw=False)
    response: Response = helpers.base_test_logic(
        API_CLIENTS.format(helpers.generate_random_int()), app, helpers.generate_request_transfer_wallet_model().json()
    )

    assert response.status_code == 424
