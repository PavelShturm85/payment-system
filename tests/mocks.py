"""Mocks for tests."""
import decimal
import typing

from payment_system import connections
from payment_system.models import entities, requests
from tests import helpers


class MockTransaction:
    """Mock db transaction."""

    def __init__(self, *_, **__):
        """Mock init."""

    async def __aenter__(self):
        """Mock aenter."""

    async def __aexit__(self, *_, **__):
        """Mock aexit."""


class MockConnection:
    """Mock connection."""

    transaction: typing.Type[MockTransaction] = MockTransaction


class BaseMockDataBase(connections.DatabaseConnection):
    """Base mock data base."""

    def __new__(cls, *_, **__):
        if not hasattr(cls, "instance"):
            cls.instance = super().__new__(cls)
        return cls.instance

    def __init__(self, *_, **__):
        """Mock init."""

    connection: MockConnection = MockConnection()  # type: ignore

    @classmethod
    async def initialize(cls) -> None:
        """Initialize connection."""

    @classmethod
    async def shutdown(cls) -> None:
        """Close connection."""

    async def connect(self) -> None:
        """Mock init connections."""

    async def disconnect(self) -> None:
        """Mock disconnect."""


class MockClientsRepository:
    """Mock Repository for client table."""

    def __init__(self, *_, error: typing.Optional[Exception] = None, **__):
        """Mock init."""
        self.error: typing.Optional[Exception] = error

    async def create_client(self, _: requests.NewClientModel) -> typing.Optional[entities.ClientModel]:
        """Create new client."""
        if not self.error:
            return helpers.generate_entities_client_model()
        raise self.error


class MockWalletsRepository:
    """Mock repository for wallets table."""

    def __init__(
        self,
        *_,
        fill_up: typing.Optional[bool] = None,
        withdraw: typing.Optional[bool] = None,
        transfer: typing.Optional[bool] = None,
        error: typing.Optional[Exception] = None,
        **__,
    ):
        """Mock init."""
        self.fill_up: typing.Optional[bool] = fill_up
        self.withdraw: typing.Optional[bool] = withdraw
        self.transfer: typing.Optional[bool] = transfer
        self.error: typing.Optional[Exception] = error

    async def fill_up_wallet(self, _: int, __: decimal.Decimal) -> bool:
        """Fill up wallet."""
        if self.error:
            raise self.error
        if self.fill_up is not None:
            return False
        return True

    async def withdraw_wallet(self, _: int, __: decimal.Decimal) -> bool:
        """Withdraw wallet."""
        if self.error:
            raise self.error
        if self.withdraw is not None:
            return False
        return True

    async def create_wallet(self, _: int, __: entities.CurrencyTypeEnum) -> typing.Optional[entities.WalletsModel]:
        """Create new wallet."""
        if not self.error:
            return helpers.generate_entities_wallet_model()
        raise self.error

    async def is_exists_wallet(self, _: int) -> None:
        """Is exists wallet."""


class MockTransactionsRepository:
    """Mock repository for transactions table."""

    async def create_transaction(self, _: int, __: decimal.Decimal, ___: typing.Optional[int] = None) -> None:
        """Create transaction."""
