"""Project-scoped test plugins."""
import fastapi
import pytest

from payment_system import api, connections, settings
from payment_system.repositories import transactions
from tests import mocks as custom_mock


@pytest.fixture(scope='module')
def app() -> fastapi.FastAPI:
    """App mock fixture."""
    app_mock: fastapi.FastAPI = fastapi.FastAPI()
    app_mock.include_router(api.ROUTER_OBJ, prefix=settings.API_PREFIX)
    app_mock.dependency_overrides[connections.DatabaseConnection] = lambda: custom_mock.BaseMockDataBase()
    app_mock.dependency_overrides[
        transactions.TransactionsRepository
    ] = lambda: custom_mock.MockTransactionsRepository()
    return app_mock
