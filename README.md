Payment system
===
This is application for performing financial transactions between clients.<br>



## Application features:
* Create a client with a USD currency wallet.
* Replenish the balance of the wallet by ID.
* Make transfers between clients by wallet ID.
* Save information about all transactions on clients wallets.


## Quick start:
1. `cd docker/local`
1. `docker-compose up`

## API documentation:
* `http://localhost/doc`

## Run tests:
* `./bin/scripts/run-tests.sh`

## Run linter:
* `./bin/scripts/run-tests.sh`

## ENV variables
* `DATABASE_DSN` - Postgres database DSN
* `DATABASE_POOL_MIN_SIZE` - database pool min size (10 by default)
* `DATABASE_POOL_MAX_SIZE` - database pool max size (30 by default)
* `DATABASE_CONNECTION_TRIES` - max connection tries for postgres (3 by default)
* `DATABASE_TRANSACTIONS_TRIES` - max tries for make transaction (3 by default)
* `DOC_PREFIX` - documentation prefix
* `API_PREFIX` - application prefix
* `UVICORN`:
   * `APP_PORT` - application port
   * `LOG_LEVEL` - log level
   * `APP_WORKERS` - amount of workers
   * `APP_LIMIT_MAX_REQUESTS` - max limit requests before restart worker
   * `APP_LOOP` - uvloop
   