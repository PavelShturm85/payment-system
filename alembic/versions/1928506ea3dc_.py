"""empty message.

Revision ID: 1928506ea3dc
Revises: fe9387269280
Create Date: 2021-04-03 23:02:39.971013
"""
import sqlalchemy as sa

from alembic import op  # type: ignore


# revision identifiers, used by Alembic.
revision = '1928506ea3dc'
down_revision = 'fe9387269280'
branch_labels = None
depends_on = None


def upgrade():
    op.create_unique_constraint('first_and_last_name_unique', 'clients', ['first_name', 'last_name'])


def downgrade():
    op.delete_unique_constraint('first_and_last_name_unique', 'clients', ['first_name', 'last_name'])
